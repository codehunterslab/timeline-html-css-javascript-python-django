from django.shortcuts import render
from .models import Event

def index(requets):
	event_list = Event.objects.order_by('year')
	context = {'event_list': event_list}
	return render(requets, 'events/index.html', context)