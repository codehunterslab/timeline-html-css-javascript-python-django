from django.db import models

class Event(models.Model):
	"""docstring for ClassName"""
	year = models.IntegerField(default=2020)
	text = models.CharField(max_length=100)